<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Chat Application</title>
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="jquery-1.12.0.min.js"></script>

</head>
<?php
    session_start();
    $fileName = "massages.txt";

    if(isset($_POST['userName'])){
        $_SESSION['user'] = $_POST['userName'];
    }
    if(isset($_SESSION['user'])){
        $UserName = $_SESSION['user'];
        if(isset($_POST['massage'])){
            $userMassage = $UserName.": ".$_POST['massage']."\n";
            $handle = fopen($fileName,"a+");
            fwrite($handle,$userMassage);
            fclose($handle);
        }
        if(isset($_REQUEST['logOut'])){
            session_destroy();
            header("location: chat.php");
        }

        $massage = file_get_contents($fileName);

        echo "<div class='chattingBlock' id='block'>
                <form method='post' action=''>
                    <p id='welMassage'>Welcome! $UserName.</p>
                    <p id='logOut'>
                        <button type='submit' name='logOut'>Log Out</button>
                    </p>
                    <textarea id='chattingText' spellcheck='false' readonly>$massage</textarea>
                    <input type='checkbox' id='autoReload' checked>
                    <p id='autoReloadText'>UnCheck To Scroll Perfectly.</p>
                    <br>
                    <input type='text' name='massage'>
                    <input type='submit' name='send' value='Send'>
                </form>
            </div>";
    }
    else{
        echo "<form action='' method='post' class='submissionBlock' id='block'>
                <input type='text' name='userName' placeholder='Type Your Name Here' required>
                <br>
                <input type='submit' name='submit' value='SUBMIT'>
            </form>";
    }
?>
<script>
    var textarea = document.getElementById('chattingText');
    $(document).ready(
        function(){
            setInterval(function(){
                $("#chattingText").load("massages.txt");
                if(document.getElementById('autoReload').checked){
                    textarea.scrollTop = textarea.scrollHeight;
                }
            }, 1000);
        }
    );
    textarea.scrollTop = textarea.scrollHeight;
</script>
<body>
</body>
</html>